(() => {
    document.documentElement.setAttribute('lang', 'ja');

	const TRANSLATES = [
        [/^\s*新規\s*$/g, 'Mới'],
        [/^\s*進行中\s*$/g, 'Đang thực hiện'],
        [/^\s*フィードバック\s*$/g, 'Feedback'],
        [/^\s*確認中\s*$/g, 'Đang xác nhận'],
        [/^\s*開発会社 テスト環境 反映待ち\s*$/g, 'Chờ phản ánh lên môi trường test'],
        [/^\s*開発会社 テスト環境 反映済み\s*$/g, 'Đã phản ánh lên môi trường test'],
        [/^\s*開発会社 テスト環境 動作確認中\s*$/g, 'Đang xác nhận hoạt động ở môi trường test'],
        [/^\s*開発会社 テスト環境 確認完了\s*$/g, 'Hoàn thành test ở môi trường test'],
        [/^\s*JPREP テスト環境 反映待ち\s*$/g, 'Chờ phản ánh lên môi trường test JPREP'],
        [/^\s*JPREP テスト環境 反映済み\s*$/g, 'Đã phản ánh lên môi trường test JPREP'],
        [/^\s*JPREP テスト環境 動作確認中\s*$/g, 'Đang xác nhận hoạt động ở môi trường test JPREP'],
        [/^\s*本番リリース待ち\s*$/g, 'Chờ release production'],
        [/^\s*本番リリース済み\s*$/g, 'Đã release production'],
        [/^\s*終了\s*$/g, 'Hoàn thành'],
        [/^\s*却下\s*$/g, 'Huỷ'],
        [/^\s*低め\s*$/g, 'Thấp'],
        [/^\s*通常\s*$/g, 'Trung'],
        [/^\s*高め\s*$/g, 'Cao'],
        [/^\s*急いで\s*$/g, 'Gấp'],
        [/^\s*今すぐ\s*$/g, 'Ngay lập tức'],
    ];

    function replace(node) {
        if (node.children.length == 0) {
			for (const tran of TRANSLATES) {
				node.innerHTML = node.innerHTML.replaceAll(tran[0], tran[1]);
			}
        } else {
            for (var children of node.children) {
                replace(children);
            }
        }
    }

	function doReplace() {
		for (const node of document.querySelectorAll('select#issue_status_id option')) {
			replace(node);
		}

		for (const node of document.querySelectorAll('select#issue_priority_id option')) {
			replace(node);
		}

		for (const node of document.querySelectorAll('#content .issue .status')) {
			replace(node);
		}

		for (const node of document.querySelectorAll('#content .issue .priority')) {
			replace(node);
		}
	}

	doReplace();

	window.__updateIssueFrom = window.updateIssueFrom;
	window.updateIssueFrom = async (...args) => {
		await window.__updateIssueFrom(...args);
		doReplace();
	};

	// Transform journal link to easy copy
	for (var a of document.querySelectorAll('a.journal-link')) {
		var url = new URL(a.href);
		url.search = '';
		a.href = url.toString();
	}
})();
